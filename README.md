# cloudcanal-issue

### 简介

此项目用于 [CloudCanal](https://www.clougence.com/) 产品收集社区用户需求、bug，交互内容以 issue 为主。

### 反馈规则

- 请先搜索相关内容，如已有相似内容的 issue，如还未满足，可跟帖反馈
- 新 issue 请按标准 需求 或 bug 模版填写相关信息
- 如 issue 内容无法解决问题或清晰沟通，请添加 CloudCanal 官网微信交流群

### 相关资料

- [CloudCanal 官网](https://www.clougence.com/)
- [CloudCanal 快速上手](https://www.clougence.com/cc-doc/quick/quick_start?src=clougence)
- [CloudCanal 常见问题](https://www.clougence.com/cc-doc/faq/cloudcanal_faq_list)
- [CloudCanal 社区版用户协议](https://www.clougence.com/cc-doc/protocol/terms_of_use)
- [CloudCanal 最佳实践系列文章](https://www.clougence.com/cc-doc/bestPractice/mysql_kafka_sync)
- [CloudCanal 开放 API 集成](https://www.clougence.com/cc-doc/blog/use_open_api_to_integrate_cc)
